from django.db import models
from django.db.models import Sum
from django.contrib.auth.models import User

from .choices import RATE_CHOICES


class Book(models.Model):
    name = models.CharField(max_length=255)
    price = models.DecimalField(max_digits=7, decimal_places=2)
    author_name = models.CharField(max_length=255)
    owner = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, related_name='user_book')
    readers = models.ManyToManyField(User, through='UserBookRelation', related_name='books')


    def __str__(self) -> str:
        return f'ID {self.id} : {self.name}'


class UserBookRelation(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    book = models.ForeignKey(Book, on_delete=models.CASCADE)
    like = models.BooleanField(default=False)
    in_bookmarks = models.BooleanField(default=False)
    rating = models.PositiveSmallIntegerField(choices=RATE_CHOICES, null=True)

    def __str__(self) -> str:
        return f'{self.user.username} -- {self.book.name} -- {self.rating}'