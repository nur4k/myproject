from django.shortcuts import render
from django_filters.rest_framework import DjangoFilterBackend
from django.db.models import Case, Count, When

from rest_framework.viewsets import ModelViewSet
from rest_framework.filters import SearchFilter, OrderingFilter
from rest_framework.mixins import UpdateModelMixin
from rest_framework.viewsets import GenericViewSet
from rest_framework.permissions import IsAuthenticated

from .serializers import BookSerializer, UserBookSerializer
from .models import Book, UserBookRelation
from .permissions import IsOwnerOrReadOnly


class BookView(ModelViewSet):
    queryset = Book.objects.all().annotate(annotade_likes=Count(Case(When(userbookrelation__like=True, then=1))))
    serializer_class = BookSerializer
    permission_classes = (IsOwnerOrReadOnly,)
    filter_backends = [DjangoFilterBackend, SearchFilter, OrderingFilter]
    filter_fields = ['price']
    search_fields = ['id', 'price']
    ordering_filter = ['name', 'price']

    def perform_create(self, serializer):
        serializer.validated_data['owner'] = self.request.user
        serializer.save()


class UserBookView(GenericViewSet,UpdateModelMixin):
    queryset = UserBookRelation.objects.all()
    serializer_class = UserBookSerializer
    permission_classes = (IsAuthenticated,)
    lookup_field = 'book' #для поиска поля (например, exactиз field_name__exact)

    def get_object(self):   #Получение обьекта по полям 'user=self.request.user, book_id=self.kwargs['book']'
        obj, _ = UserBookRelation.objects.get_or_create(user=self.request.user,
                                                        book_id=self.kwargs['book'])
        print(obj)


def oauth(request):
    return render(request, 'index.html')
