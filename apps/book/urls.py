from rest_framework.routers import DefaultRouter as DR
from rest_framework.urls import path

from apps.book import views


router = DR()

router.register('book', views.BookView, basename='book')
router.register('book/user', views.UserBookView, basename='book/user')

urlpatterns = [
    path('auth/', views.oauth)
]

urlpatterns += router.urls
