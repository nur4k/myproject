from rest_framework import serializers
from .models import Book, UserBookRelation


class BookSerializer(serializers.ModelSerializer):
    likes_count = serializers.SerializerMethodField()

    class Meta:
        model = Book
        fields = ('id', 'name', 'price', 'author_name', 'likes_count')

    #подсчет лайков с помощью кастомного метода, к названинию метода добавляем -get_- !!!
    def get_likes_count(self, instance):
        return UserBookRelation.objects.filter(book=instance, like=True).count()


class UserBookSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserBookRelation
        fields = ('id', 'book', 'like', 'in_bookmarks', 'rating')
